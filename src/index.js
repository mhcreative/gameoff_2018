import 'phaser';

import { Boot } from 'scenes/boot';
import { PlaygroundScene } from 'scenes/playground';
import { IntroScene } from 'scenes/intro';
import { HouseScene } from 'scenes/house';

import gamestore from 'data/gamestore.js';

var config = {
  type: Phaser.AUTO,
  width: gamestore.gamedata.canvasWidth,// width of canvas
  height: gamestore.gamedata.canvasHeight,// height of canvas
  physics: {// configure physics
      default: gamestore.physics.default,
      arcade: {
          gravity: { y: gamestore.physics.gravity }, // Higher number means more intense gravity
          debug: gamestore.physics.debug
      }
  },
  scene: [Boot, PlaygroundScene, IntroScene, HouseScene]
};
var game = new Phaser.Game(config);
