import 'phaser';
import gamestore from 'data/gamestore';
import CharacterMovement from 'modules/character-movement.js';
import MapSetup from 'modules/map-setup.js';
import Entities from 'modules/entities.js';
import CollectibleSetup from 'modules/collectible-setup.js';
import Npcs from 'modules/Npcs.js';
import CameraSetup from 'modules/camera-setup.js';
import Audio from 'modules/audio.js';
import Particles from 'modules/particles';
import Zones from 'modules/zones';

const mapSetup = new MapSetup()
const entities = new Entities()
const collectibleSetup = new CollectibleSetup()
const characterMovement = new CharacterMovement()
const npcs = new Npcs()
const cameraSetup = new CameraSetup()
const audio = new Audio()
const particles = new Particles()
const zones = new Zones()

export class PlaygroundScene extends Phaser.Scene {
constructor () {
  super('PlaygroundScene')
  this.gamedata = gamestore.gamedata;
  this.mapData = gamestore.mapData;
  this.entities = gamestore.entities;
}

  preload() {
    gamestore.gameplayStates.failed = false;
  }

  create() {

    // Setup up map for this scene/level
    const mapObj = mapSetup.sceneMap(this, this.mapData.playground)
    this.map = mapObj.map,
    this.groundTiles = mapObj.groundTiles,
    this.groundLayer = mapObj.groundLayer;

    // this.itemLayer = collectibleSetup.setupItems (this, 'coin', 'coins', 41);

    // Set up entities
    this.player = entities.entitySetup(
      this,
      this.map,
      this.entities.catHero.spawnPoint,
      this.entities.catHero.spriteKey,
      this.entities.catHero.physics,
      this.entities.catHero.bounce,
      this.entities.catHero.setCollideWorldBounds,
      true
    )

    // this.player.setSize(39, 55, true)
    // this.player.body.offset.y = 25;
    // entities.setHitBox({
    //   size: {
    //     width: 39,
    //     height: 55
    //   },
    //   offset: {
    //     x: 0,
    //     y: 25
    //   }
    // }, this.player);

    this.jumpParticles = this.add.particles('blue');

    this.enemy1 = entities.entitySetup(
      this,
      this.map,
      this.entities.koopa.spawnPoint,
      this.entities.koopa.spriteKey,
      this.entities.koopa.physics,
      this.entities.koopa.bounce,
      true,
      true
    )
    this.enemy1.anims.play('patrol')
    npcs.entityPatrol(this, this.enemy1);
    this.bombs = this.physics.add.group()
    // this.enemy1.projectile = npcs.enemyProjectile(this, this.enemy1)

    this.boobyTrap = entities.entitySetup(
      this,
      this.map,
      this.entities.boobyTrap.spawnPoint,
      this.entities.boobyTrap.spriteKey,
      this.entities.boobyTrap.physics,
      this.entities.boobyTrap.bounce,
      true,
      true
    )
    this.boobyTrap.setTint(0xff0000)
    this.boobyTrap.body.allowGravity = false;

    // Add Sound Effects to scene
    this.sfx = audio.addSFX(this)

    // Setup Camera
    this.cameraMain = cameraSetup.camSetup(
      this,
      this.map,
      this.gamedata.defaultCameraBgColour
    )

    // Set up vasic collision detection for ground layer
    this.physics.add.collider(
      this.player,
      this.groundLayer
    )

    this.physics.add.overlap(
      this.player,
      this.itemLayer
    )

    this.physics.add.collider(
      this.enemy1,
      this.groundLayer
    )

    // Set up collider methods
    this.physics.add.overlap(
      this.player,
      this.boobyTrap,
      npcs.entityDestroy.bind(this, [this.enemy1, this.boobyTrap], 'highJump'),
      null,
      this
    )

    this.physics.add.collider(
      this.player,
      this.enemy1,
      npcs.entityDestroy.bind(this, [this.enemy1], 'highJump'),
      null,
      this
    )

    this.physics.add.collider(
      this.player,
      this.bombs,
      npcs.hitBomb.bind(this)
    )

    // Setup button input
    this.cursors = this.input.keyboard.createCursorKeys();

  }// create

  update () {
    characterMovement.buttonInputHandler(
      this,
      this.cursors,
      this.player,
      this.sfx
    )
  }

}
