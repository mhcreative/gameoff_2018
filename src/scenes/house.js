import 'phaser';
import gamestore from 'data/gamestore';
import CharacterMovement from 'modules/character-movement.js';
import MapSetup from 'modules/map-setup.js';
import Entities from 'modules/entities.js';
import CollectibleSetup from 'modules/collectible-setup.js';
import Npcs from 'modules/Npcs.js';
import CameraSetup from 'modules/camera-setup.js';
import Audio from 'modules/audio.js';
import Particles from 'modules/particles';
import Zones from 'modules/zones';
import GameplayStates from 'modules/gameplay-states';

const mapSetup = new MapSetup()
const entities = new Entities()
const collectibleSetup = new CollectibleSetup()
const characterMovement = new CharacterMovement()
const npcs = new Npcs()
const cameraSetup = new CameraSetup()
const audio = new Audio()
const particles = new Particles()
const zones = new Zones()
const gameplayStates = new GameplayStates()

export class HouseScene extends Phaser.Scene {
constructor () {
  super('HouseScene')
  this.gamedata = gamestore.gamedata;
  this.mapData = gamestore.mapData;
  this.entities = gamestore.entities;
}

  preload() {
    gamestore.gameplayStates.failed = false;
    gamestore.gameplayStates.success = false;
  }

  create() {

    const self = this;
    // Setup up map for this scene/level
    const mapObj = mapSetup.sceneMap(this, this.mapData.house)
    this.map = mapObj.map,
    this.groundTiles = mapObj.groundTiles,
    this.groundLayer = mapObj.groundLayer;

    // this.itemLayer = collectibleSetup.setupItems (this, 'coin', 'coins', 141);

    // Set up entities
    this.exit = entities.entitySetup(
      this,
      this.map,
      this.entities.exit.spawnPoint,
      this.entities.exit.spriteKey,
      this.entities.exit.physics,
      this.entities.exit.bounce,
      true,
      true
    )

    this.boobyTrap = entities.entitySetup(
      this,
      this.map,
      this.entities.boobyTrap.spawnPoint,
      this.entities.boobyTrap.spriteKey,
      this.entities.boobyTrap.physics,
      this.entities.boobyTrap.bounce,
      true,
      true
    )
    this.boobyTrap.setTint(0xff0000)
    this.boobyTrap.body.allowGravity = false;
      
    this.player = entities.entitySetup(
      this,
      this.map,
      this.entities.catHero.spawnPoint,
      this.entities.catHero.spriteKey,
      this.entities.catHero.physics,
      this.entities.catHero.bounce,
      this.entities.catHero.setCollideWorldBounds,
      true
    )

    this.jumpParticles = this.add.particles('blue');

    this.enemy1 = entities.entitySetup(
      this,
      this.map,
      this.entities.dm.spawnPoint,
      this.entities.dm.spriteKey,
      this.entities.dm.physics,
      this.entities.dm.bounce,
      true,
      true
    )
    this.enemy1.anims.play('dmPatrol')
    npcs.entityPatrol(this, this.enemy1);
    this.bombs = this.physics.add.group()

    this.bat = entities.entitySetup(
      this,
      this.map,
      this.entities.bat.spawnPoint,
      this.entities.bat.spriteKey,
      this.entities.bat.physics,
      this.entities.bat.bounce,
      true,
      true
    )
    this.bat.anims.play('batPatrol')
    npcs.entityPatrol(this, this.bat);
    this.bat.body.allowGravity = false;
    // this.enemy1.projectile = npcs.enemyProjectile(this, this.enemy1)
    

    // Add Sound Effects to scene
    this.sfx = audio.addSFX(this)

    // Setup Camera
    this.cameraMain = cameraSetup.camSetup(
      this,
      this.map,
      this.gamedata.defaultCameraBgColour
    )

    // Set up vasic collision detection for ground layer
    this.physics.add.collider(
      this.player,
      this.groundLayer
    )

    this.physics.add.overlap(
      this.player,
      this.itemLayer
    )

    this.physics.add.collider(
      this.groundLayer,
      this.exit
    )

    this.physics.add.collider(
      this.enemy1,
      this.groundLayer
    )

    // Set up collider methods
    this.physics.add.overlap(
      this.player,
      this.boobyTrap,
      npcs.entityDestroy.bind(this, [this.enemy1], 'highJump', 'pickup'),
      null,
      this
    )

    this.physics.add.collider(
      this.player,
      this.enemy1,
      gameplayStates.failState.bind(this, this, this.cameraMain),
      null,
      this
    )

    this.physics.add.collider(
      this.player,
      this.bat,
      gameplayStates.failState.bind(this, this, this.cameraMain),
      null,
      this
    )

    this.physics.add.overlap(
      this.player,
      this.exit,
      gameplayStates.successState.bind(this, this, this.cameraMain),
      null,
      this
    )

    this.physics.add.collider(
      this.player,
      this.bombs,
      npcs.hitBomb.bind(this)
    )

    // Setup button input
    this.cursors = this.input.keyboard.createCursorKeys();

    this.sfx.house.play();

    // this.texttest = this.add.text(50, 50).setText('Click to move').setScrollFactor(0);

  }// create

  update () {
    characterMovement.buttonInputHandler(
      this,
      this.cursors,
      this.player,
      this.sfx
    )
  }

}
