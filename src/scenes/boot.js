import 'phaser';
import gamestore from 'data/gamestore';
import Entities from 'modules/entities.js';
import Audio from 'modules/audio.js';

const entities = new Entities()
const audio = new Audio()

export class Boot extends Phaser.Scene {

constructor () {
  super('Boot')
  this.playerWidth = gamestore.physics.playerWidth;
  this.playerHeight = gamestore.physics.playerHeight;
  this.mapData = gamestore.mapData;
  this.entities = gamestore.entities;
}

  preload() {
    const progressBar = this.add.graphics();
    const progressBox = this.add.graphics();
    progressBox.fillStyle(0xffffff, 0.8);
    progressBox.fillRect(20, 20, 320, 50);

    const width = this.cameras.main.width;
    const height = this.cameras.main.height;

    var loadingText = this.make.text({
        x: 20,
        y: 75,
        text: 'Loading...',
        style: {
            font: '20px monospace',
            fill: '#ffffff'
        }
    });
    loadingText.setOrigin(0, 0);

    this.load.on('progress', function (value) {
      // console.log(value);
      progressBar.clear();
      progressBar.fillStyle(0xffffff, 1);
      progressBar.fillRect(30, 30, 300 * value, 30);
    })

    this.load.on('fileprogress', function (file) {
        // console.log(file.src);
    })

    this.load.on('complete', function () {
        progressBar.destroy();
        progressBox.destroy();
        loadingText.destroy();
    })

    this.load.image('bomb', 'assets/objects/bomb.png')
    this.load.image('blue', 'assets/objects/blue.png')
    this.load.image('exit', 'assets/objects/exit.png')
    this.load.image('successText', 'assets/objects/success.png')
    this.load.image('trigger', 'assets/objects/trigger.png')

    this.load.spritesheet('wizardIdle',
        'assets/characters/wizard-idle.png',
        { frameWidth: 160, frameHeight: 160 }
    )

    this.load.spritesheet('catIdle',
        'assets/characters/cat-idle.png',
        { frameWidth: 38, frameHeight: 30 }
    )

    this.load.spritesheet('catWalk',
        'assets/characters/cat-walk.png',
        { frameWidth: 36, frameHeight: 30 }
    )

    this.load.spritesheet('catHeroIdle',
        'assets/characters/cat-hero-idle.png',
        { frameWidth: 39  , frameHeight: 80 }
    )

    this.load.spritesheet('catHeroWalk',
        'assets/characters/cat-hero-walk.png',
        { frameWidth: 48  , frameHeight: 80 }
    )

    this.load.spritesheet('catHeroRoll',
        'assets/characters/cat-hero-roll.png',
        { frameWidth: 35  , frameHeight: 80 }
    )

    this.load.spritesheet('catHeroSuper',
        'assets/characters/cat-hero-super.png',
        { frameWidth: 72  , frameHeight: 84 }
    )

    this.load.spritesheet('catHeroJump',
        'assets/characters/cat-hero-jump.png',
        { frameWidth: 50  , frameHeight: 80 }
    )

    this.load.spritesheet(this.entities.dust.spriteKey, 'assets/objects/dust.png', { frameWidth: 35, frameHeight: 29 })

    this.load.spritesheet('jumpDust', 'assets/objects/jumpDust3.png', { frameWidth: 90, frameHeight: 186 })

    this.load.spritesheet('koopa', 'assets/objects/koopa.png', { frameWidth: 32, frameHeight: 56 })

    this.load.spritesheet('dm', 'assets/characters/dm.png', { frameWidth: 23, frameHeight: 29 })

    this.load.spritesheet('bat', 'assets/characters/bat.png', { frameWidth: 34, frameHeight: 45 })

    // Maps made with Tiled in JSON format
    this.load.tilemapTiledJSON(this.mapData.playground.tilemapKey, this.mapData.playground.jsonFilePath);

    this.load.tilemapTiledJSON(this.mapData.intro.tilemapKey, this.mapData.intro.jsonFilePath);

    this.load.tilemapTiledJSON(this.mapData.house.tilemapKey, this.mapData.house.jsonFilePath);

    this.load.spritesheet(// tiles in spritesheet
      this.mapData.playground.spriteSheetKey, this.mapData.playground.spriteSheetFilePath,
      {frameWidth: this.mapData.playground.frameWidth, frameHeight: this.mapData.playground.frameHeight})

    this.load.spritesheet(// tiles in spritesheet
      this.mapData.intro.spriteSheetKey, this.mapData.intro.spriteSheetFilePath,
      {frameWidth: this.mapData.intro.frameWidth, frameHeight: this.mapData.intro.frameHeight})

      this.load.spritesheet(// tiles in spritesheet
        this.mapData.house.spriteSheetKey, this.mapData.house.spriteSheetFilePath,
        {frameWidth: this.mapData.house.frameWidth, frameHeight: this.mapData.house.frameHeight})

    // Webfont Setup
    this.load.script('webfont', gamestore.scenes.dialogueData.webfont);

    // Sound FX
    audio.loadSFX(this)
  }

  create() {
    entities.entityAnimation(this)
    this.scene.start('IntroScene')
  }// create

}