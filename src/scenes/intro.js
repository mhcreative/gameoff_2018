import 'phaser';
import gamestore from 'data/gamestore';
import MapSetup from 'modules/map-setup.js';
import Entities from 'modules/entities.js';
import CollectibleSetup from 'modules/collectible-setup.js';
import CameraSetup from 'modules/camera-setup.js';
import Audio from 'modules/audio.js';
import Dialogue from 'modules/dialogue';

const mapSetup = new MapSetup()
const entities = new Entities()
const collectibleSetup = new CollectibleSetup()
const cameraSetup = new CameraSetup()
const audio = new Audio()
const dialogue = new Dialogue()

export class IntroScene extends Phaser.Scene {
constructor () {
  super('IntroScene')
  this.gamedata = gamestore.gamedata;
  this.mapData = gamestore.mapData;
  this.entities = gamestore.entities;
  this.dialogue = gamestore.scenes;
  this.skip = false;
}

  preload() {
    gamestore.gameplayStates.failed = false;
    this.skip = false;
  }

  create() {

    const self = this;

    // Setup up map for this scene/level
    const mapObj = mapSetup.sceneMap(this, this.mapData.intro)
    this.map = mapObj.map,
    this.groundTiles = mapObj.groundTiles,
    this.groundLayer = mapObj.groundLayer;

    // Set up entities
    this.player = entities.entitySetup(
      this,
      this.map,
      this.entities.player.spawnPoint,
      this.entities.player.spriteKey,
      this.entities.player.physics,
      this.entities.player.bounce,
      this.entities.player.setCollideWorldBounds,
      false
    )

    this.cat = entities.entitySetup(
      this,
      this.map,
      this.entities.cat.spawnPoint,
      this.entities.cat.spriteKey,
      this.entities.cat.physics,
      this.entities.cat.bounce,
      this.entities.cat.setCollideWorldBounds,
      true
    )

    this.catHero = entities.entitySetup(
      this,
      this.map,
      this.entities.catHero.spawnPoint,
      this.entities.catHero.spriteKey,
      this.entities.catHero.physics,
      this.entities.catHero.bounce,
      this.entities.catHero.setCollideWorldBounds,
      false
    )

    // Setup Camera
    this.cameraMain = cameraSetup.camSetup(
      this,
      this.map,
      this.gamedata.defaultCameraBgColour
    )
    this.cameraMain.startFollow(this.cat);

    // Setup Animation
    this.timelineIntro = this.tweens.createTimeline();

    this.timelineIntro.add({
        targets: this.cat,
        x: "+=100",
        ease: 'Power1',
        duration: 1500,
        onStart: function () {
          self.cat.anims.play('catWalkR', true)
        },
        onComplete: function () {
          // console.log('cat walked');
          self.dialogueCat = dialogue.generateDialogueBox(
            self,
            self.dialogue.intro.dialogueCat,
            self.dialogue.intro.dialogueCat.script.line1,
            self.dialogue.dialogueData.catConfig
          );
        }
    });

    this.timelineIntro.add({
        targets: this.cat,
        x: "+=0",
        ease: 'Power1',
        duration: 1100,
        onStart: function () {
          // console.log('cat idle');
          self.cat.anims.play('catIdleR', true)
        },
        onComplete: function () {
          // console.log('cat sits');
          self.cat.anims.play('catSitR', true)
        }
    });

    this.timelineIntro.add({
        targets: this.cat, x: "+=0",
        duration: 1000,
        onComplete: function () {
          // console.log("Wizard speaks");
          dialogue.hideDialogueBox(self.dialogueCat)
          self.dialogueWizard = dialogue.generateDialogueBox(
            self,
            self.dialogue.intro.dialogueWizard,
            self.dialogue.intro.dialogueWizard.script.line1,
            self.dialogue.dialogueData.WizardConfig
          );
        }
    });

    this.timelineIntro.add({
        targets: this.cat, x: "+=0",
        duration: 2000,
        onComplete: function () {
          // console.log("Cat speaks ");
          dialogue.showDialogueBox(self.dialogueCat)
          dialogue.hideDialogueBox(self.dialogueWizard)
        }
    });

    this.timelineIntro.add({
        targets: this.cat, x: "+=0",
        duration: 3000,
        onComplete: function () {
          // console.log("Wizard speaks warning");
          dialogue.hideDialogueBox(self.dialogueCat)
          self.dialogueWizard = dialogue.generateDialogueBox(
            self,
            self.dialogue.intro.dialogueWizard,
            self.dialogue.intro.dialogueWizard.script.line2,
            self.dialogue.dialogueData.WizardConfig
          );
        }
    });

    this.timelineIntro.add({
        targets: this.cat, x: "+=0",
        duration: 3000,
        onComplete: function () {
          // console.log("Cat agrees ");
          dialogue.showDialogueBox(self.dialogueCat)
          dialogue.hideDialogueBox(self.dialogueWizard)
        }
    });

    this.timelineIntro.add({
        targets: this.cat, x: "+=0",
        duration: 3000,
        onComplete: function () {
          // console.log("Wizard agrees");
          dialogue.hideDialogueBox(self.dialogueCat)
          self.dialogueWizard = dialogue.generateDialogueBox(
            self,
            self.dialogue.intro.dialogueWizard,
            self.dialogue.intro.dialogueWizard.script.line3,
            self.dialogue.dialogueData.WizardConfig
          );
        }
    });

    this.timelineIntro.add({
        targets: this.cat,
        x: "+=0",
        delay: 2000,
        duration: 1000,
        onComplete: function () {
          self.cameraMain.fade(250, 255, 255, 255)
        }
    });

    this.timelineIntro.add({
        targets: this.cat,
        alpha: 0,
        duration: 500,
        onStart: function () {
          dialogue.hideDialogueBox(self.dialogueWizard)
          self.dialogue.intro.dialogueWizard.position.y = 290;
        },
        onComplete: function () {
          // console.log('cat is levitating');
          self.catHero.setDepth(1.5);
          self.catHero.anims.play('catHeroSuperR', true)
          self.catHero.x = self.cat.x;
          self.catHero.y = self.cat.y-70;
          self.catHero.alpha = 1;
          self.catHero.body.allowGravity = false;
          self.cameraMain.fadeFrom(250, 255, 255, 255)
          self.dialogueWizard = dialogue.generateDialogueBox(
            self,
            self.dialogue.intro.dialogueWizard,
            self.dialogue.intro.dialogueWizard.script.line4,
            self.dialogue.dialogueData.WizardConfig
          );
        }
    });

    this.timelineIntro.add({
        targets: this.catHero,
        delay: 250,
        y: "-=30",
        ease: 'Power2.easeInOut',
        duration: 1000,
        repeat: 1,
        yoyo: true,
        onComplete: function () {
          console.log('cat is hero')
          entities.setHitBox({
            size: {
              width: gamestore.entities.player.playerHitboxConfig.running.size.width,
              height: gamestore.entities.player.playerHitboxConfig.running.size.height
            },
            offset: {
              x: gamestore.entities.player.playerHitboxConfig.running.offset.x,
              y: gamestore.entities.player.playerHitboxConfig.running.offset.y
            }
          }, self.catHero);
          self.catHero.anims.play('running', true)
          self.catHero.body.allowGravity = true;
        }
    });

    this.timelineIntro.add({
        targets: this.catHero,
        delay: 750,
        y: "-=0",
        x: "+=500",
        ease: 'Power2.easeInOut',
        duration: 2000,
        onStart: function () {
          self.catHero.anims.play('running', true)
        },
        onComplete: function () {
          console.log('cat has left frame');
          self.cameraMain.fade(1000)
          setTimeout(function(){
            self.scene.start('HouseScene')
          },1250)
        }
    });

    this.timelineIntro.play();

    this.wizard = entities.entitySetup(
      this,
      this.map,
      this.entities.wizard.spawnPoint,
      this.entities.wizard.spriteKey,
      this.entities.wizard.physics,
      this.entities.wizard.bounce,
      this.entities.wizard.setCollideWorldBounds,
      true
    )

    this.wizard.anims.play('wizardIdle')

    // Add Sound Effects to scene
    this.sfx = audio.addSFX(this)

    // Set up vasic collision detection for ground layer
    this.physics.add.collider(
      [
        this.player,
        this.cat,
        this.catHero
      ],
      this.groundLayer
    )

    this.texttest = this.add.text(20, 320).setText('Hit UP to skip.').setScrollFactor(0);


    this.cursors = this.input.keyboard.createCursorKeys();

  }// create

  update () {
    const self = this;
    if (this.cursors.up.isDown) {
      if (!this.skip) {
        this.skip = true;
        this.cameraMain.fade(1000)
        setTimeout(function(){
          self.scene.start('HouseScene')
        },1050)
      }
    }
  }

}
