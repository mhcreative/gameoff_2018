import 'phaser';
import gamestore from 'data/gamestore';

export default class Entities {

  entitySetup (ctx, map, spawnPoint, spriteKey, physics, bounce, setCollideWorldBounds, startVisible) {
    let entity;
    let spawn;
    let x = 600;
    let y = 250;

    if (spawnPoint !== '' && typeof map !== 'undefined') {
      spawn = map.findObject("objects", obj => obj.name === spawnPoint);
      x = spawn.x;
      y = spawn.y;
    }

    if (physics) {
      entity = ctx.physics.add.sprite(x, y, spriteKey);
      entity.setBounce(bounce);
      entity.setCollideWorldBounds(setCollideWorldBounds);
    } else {
      entity = ctx.add.sprite(x, y, spriteKey);
    }

    startVisible === true ? entity.alpha = 1 : entity.alpha = 0;

    return entity
  }

  entityAnimation (ctx) {
    // Player running animations
    const runningFrameRate = gamestore.physics.runningFrameRate;

    this.setAnimation(ctx, 'idle', 'catHeroIdle', 0, 3, 7, -1, false); // Idle
    this.setAnimation(ctx, 'running', 'catHeroWalk', 0, 5, runningFrameRate, -1, false); // Running
    this.setAnimation(ctx, 'jumping', 'catHeroRoll', 2, 2, 1, -1, false); // Jumping
    this.setAnimation(ctx, 'skid', 'catHeroWalk', 2, 2, 1, -1, false); // Skidding
    this.setAnimation(ctx, 'screwball', 'catHeroRoll', 4, 6, runningFrameRate, -1, true);

    // Dust animations
    this.setAnimation(ctx, 'runDust', 'dust', 0, 7, 10, 0, false); // Running dust
    this.setAnimation(ctx, 'highJump', 'jumpDust', 0, 10, 10, 0, false); // Jumping dust

    // Enemy Animations
    this.setAnimation(ctx, 'patrol', 'koopa', 0, 3, 4, -1, false); // Koopa Patrol

    this.setAnimation(ctx, 'wizardIdle', 'wizardIdle', 3, 7, 7, -1, true);

    this.setAnimation(ctx, 'catIdleR', 'catIdle', 1, 4, 2, 0, false);
    this.setAnimation(ctx, 'catSitR', 'catIdle', 0, 0, 2, 0, false);
    this.setAnimation(ctx, 'catWalkR', 'catWalk', 0, 5, 2, -1, false);

    this.setAnimation(ctx, 'catHeroSuperR', 'catHeroSuper', 0, 2, 10, -1, true);
    this.setAnimation(ctx, 'catHeroSlamR', 'catHeroSuper', 3, 5, 10, -1, true);
    this.setAnimation(ctx, 'catHeroNeutralAir', 'catHeroJump', 6, 6, 10, 0, true);

    this.setAnimation(ctx, 'dmPatrol', 'dm', 0, 2, 10, -1, true);

    this.setAnimation(ctx, 'batPatrol', 'bat', 0, 2, 10, -1, true);
    
  }

  setAnimation (ctx, key, sprite, start, end, frameRate, repeat, yoyo) {
    ctx.anims.create({
        key: key,
        frames: ctx.anims.generateFrameNumbers(sprite, { start: start, end: end }),
        frameRate: frameRate,
        repeat: repeat,
        yoyo: yoyo ? true : false
    })
  }

  setHitBox (config, entity) {
    entity.setSize(config.size.width, config.size.height, true)
    entity.body.offset.x = config.offset.x;
    entity.body.offset.y = config.offset.y;    
  }

}
