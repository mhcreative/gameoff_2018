import 'phaser';
import gamestore from 'data/gamestore.js';

export default class Dialogue {

    constructor () {
      this.dialogueData = gamestore.scenes.dialogueData;
    }

    webfontSetup (webfont) {
    WebFont.load({
        google: {
            families: this.scenesData.dialogueData.fontFamilies
        },
        active: function ()
        {
            textModalVar = textModal(self, text1, 'Contrail One');
        }
    });
    }

    generateDialogueBox (ctx, textBox, script, characterConfig) {

      var dialogueText = ctx.add.text(textBox.position.x, textBox.position.y, script, {
        fontFamily: this.dialogueData.webSafeFont,
        fontSize: 16,
        lineSpacing: 10,
        color: characterConfig.colour,
        backgroundColor: characterConfig.bgColour,
        wordWrap: {
          width: 320,
          useAdvancedWrap: true
        },
        shadow: {
          color: '0000000',
          fill: true,
          offsetX: 2,
          offsetY: 2,
          blur: 8
        },
        padding: {
          left: 15,
          top: 15
        }
      });

      const dialogueTextBox = ctx.add.rectangle(dialogueText.x, dialogueText.y, dialogueText.width, dialogueText.height, 0xff6699).setOrigin(0,0);
      dialogueTextBox.setStrokeStyle(10, 0xffffff);
      dialogueTextBox.setDepth(1);
      dialogueText.setDepth(1.1);
      dialogueText.alpha = textBox.visible ? 1 : 0;
      dialogueTextBox.alpha = textBox.visible ? 1 : 0;

      return {dialogueText:dialogueText, dialogueTextBox: dialogueTextBox};

    }

    hideDialogueBox (obj) {
      for (var key in obj) {
        obj[key].visible = false;
      }
    }

    showDialogueBox (obj) {
      for (var key in obj) {
        obj[key].visible = true;
      }
    }

}
