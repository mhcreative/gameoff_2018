import 'phaser';
import gamestore from 'data/gamestore.js';

export default class Audio {

  constructor () {
    this.sfx = gamestore.audio.sfx
  }

  // Load sound effects in the preload function of your scene
  loadSFX (ctx) {
    this.sfx.forEach((fx) => { // Go through array and pre-load each SFX file into scene
      fx.sound = ctx.load.audio(fx.name, fx.src)
    })
    return this.sfx
  }

  // Add each SFX to scene - Reference name key in gamestore audio.sfx array when playing sound. e.g. sfx.jump2.play();
  addSFX (ctx) {
    var obj = {} // Object to be returned
    this.sfx.forEach((fx) => {// Go through array and add each SFX to scene
      var sfx = ctx.sound.add(fx.name, {
        volume: fx.volume,
        loop: fx.loop
        }
      )
      obj[fx.name] = sfx;
    })
    return obj
  }

}
