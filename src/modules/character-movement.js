import 'phaser';
import gamestore from 'data/gamestore';
import Particles from 'modules/particles';
import GameplayStates from 'modules/gameplay-states';
import Entities from 'modules/entities.js';

const particles = new Particles()
const entities = new Entities()
const gameplayStates = new GameplayStates()

export default class CharacterMovement {

  constructor () {
    this.physics = gamestore.physics;
    this.runAcceleration = gamestore.physics.runAcceleration;
    this.runDeceleration = gamestore.physics.runDeceleration;
    this.facingRight = gamestore.physics.facingRight;
    this.jumpVelocity = gamestore.physics.jumpVelocity;
    this.superJumpVelocity = gamestore.physics.superJumpVelocity;
    this.skidFlipVelocity = gamestore.physics.skidFlipVelocity;
    this.wallJumpVelocity = gamestore.physics.wallJumpVelocity;
    this.jumped = gamestore.physics.jumped; // Player is holding jump button
    this.running = false;
    this.skidding = false;
    this.player;
    this.playerHitboxConfig = gamestore.entities.player.playerHitboxConfig;
  }

  runDust (ctx, player) { //Animate dust on player running
    const duster = ctx.add.sprite(player.x - 30, player.y + 5, 'dust');
    if (this.facingRight) {
      duster.x = player.x - 30;
      duster.flipX = true;
    } else {
      duster.x = player.x + 30;
      duster.flipX = false;
    }
    this.setDirection('runDust', duster);
    duster.on(
      'animationcomplete',
      function () {duster.destroy()},
      this
    );
    this.running = true;
  }

  jumpDust (ctx, player) {
    const duster = ctx.add.sprite(player.x, player.y - 75, 'jumpDust');
    if (this.facingRight) {
      duster.flipX = true;
    } else {
      duster.flipX = false;
    }
    this.setDirection('highJump', duster);
    duster.on(
      'animationcomplete',
      function () {duster.destroy()},
      this
    );
  }

  faceCharacter (player) {
    this.setDirection('idle', player)
    if (!this.facingRight) {
      player.flipX = true;
    } else {
      player.flipX = false;
    }
  }

  // Movement based on key inputs
  buttonInputHandler (ctx, cursors, player, sfx) {
    if (cursors.right.isDown) {
      // console.log("moving right")

      this.facingRight = true; //Player is facing right
      !this.running ? this.runDust(ctx, player) : null;
      this.setDirection('running', player); //Change the sprite animation to the right-running loop

      if (player.body.velocity.x < 0) {// If the velocity is less than 0
        player.setAccelerationX(this.runDeceleration*2)//decelerate quickly because the player is trying to switch directions
        this.setDirection('skid', player);// Player skids until velocity reaches 0
        this.skidding = true;
        player.flipX = false;
        if (cursors.up.isDown && player.body.onFloor()) {
          player.setVelocity(this.skidFlipVelocity.x, -this.skidFlipVelocity.y)
          this.jumpDust(ctx, player)
          player.flipX = false;
        }
      } else {
        player.setAccelerationX(this.runAcceleration)// change accelertaion to neutral value(250)
        entities.setHitBox({
          size: {
            width: this.playerHitboxConfig.running.size.width,
            height: this.playerHitboxConfig.running.size.height
          },
          offset: {
            x: this.playerHitboxConfig.running.offset.x,
            y: this.playerHitboxConfig.running.offset.y
          }
        }, player);
        player.flipX = false;
        this.skidding = false;
      }

    } else if (cursors.left.isDown) {

      this.facingRight = false;
      !this.running ? this.runDust(ctx, player) : null;

      if (player.body.velocity.x > 0) {
        player.setAccelerationX(-(this.runDeceleration*2))
        this.setDirection('skid', player);
        this.skidding = true;
        player.flipX = true;
        if (cursors.up.isDown && player.body.onFloor()) {
          player.setVelocity(-this.skidFlipVelocity.x, -this.skidFlipVelocity.y)
          this.jumpDust(ctx, player)
          player.flipX = true;
        }
      } else {
        player.setAccelerationX(-this.runAcceleration)
        this.setDirection('running', player);
        player.flipX = true;
        this.skidding = false;
      }

    } else { // The player is not pressing anything
      this.faceCharacter(player)
      entities.setHitBox({
        size: {
          width: this.playerHitboxConfig.idle.size.width,
          height: this.playerHitboxConfig.idle.size.height
        },
        offset: {
          x: this.playerHitboxConfig.idle.offset.x,
          y: this.playerHitboxConfig.idle.offset.y
        }
      }, player);
      player.setAccelerationX(0).setDragX(this.runDeceleration)
      if (player.body.onFloor()) {
        this.running = false;
      }

    }

    // Jumping
    if (cursors.up.isDown && player.body.onFloor()) { // Player pressing up and player is on ground
      if (!this.jumped) {
        this.jumped = true;
        if (gamestore.physics.superJump) {
          player.setVelocityY(-this.superJumpVelocity);
          gamestore.physics.superJump = false;
          this.jumpDust(ctx, player)
        } else {
          if (!this.skidding) {
            player.setVelocityY(-this.jumpVelocity);
          }
          sfx.jump.play();
          this.jumped ? particles.jumpParticles(ctx, player) : null;
        }
      }
      this.running = true;
    }
    if (
      cursors.up.isDown && !player.body.onFloor() ||
      cursors.up.isUp && !player.body.onFloor()
    ) { // Player pressing left/right when airborne
      entities.setHitBox({
        size: {
          width: this.playerHitboxConfig.jumping.size.width,
          height: this.playerHitboxConfig.jumping.size.height
        },
        offset: {
          x: this.playerHitboxConfig.jumping.offset.x,
          y: this.playerHitboxConfig.jumping.offset.y
        }
      }, player);
      if (this.facingRight) {
        this.setDirection('jumping', player);
        // console.log('jumping')
        player.setOrigin(0.5, 0.75)
        player.rotation += 0.3;
      } else {
        this.setDirection('jumping', player);
        player.rotation -= 0.3;
      }
    }
    if (!cursors.up.isDown && !player.body.onFloor()) { // Player pressing nothing when airborne
      if (this.facingRight) {
        this.setDirection('jumping', player);
      } else {
        this.setDirection('jumping', player);
      }
    }
    if (cursors.up.isUp && player.body.onFloor()) {// Prevent instant jump repeat on landing
      this.jumped = false;
    }
    if (player.body.onFloor()) {
      player.setBounce(gamestore.entities.player.bounce)
      player.rotation = 0;
    }

    if (ctx.player.y >= (ctx.scene.scene.physics.world.bounds.bottom - ctx.player.height/2)) {
      gameplayStates.failState(ctx, ctx.cameraMain);
    }

    // Wallgrab
    // if (cursors.right.isDown && player.body.onWall() && !player.body.onFloor() || cursors.left.isDown && player.body.onWall() && !player.body.onFloor()) {//If player is in air and moving against a wall
    //   player.setVelocity(0)// Keep player in place
    //   player.rotation = 0;
    //   // console.log('wall grab')
    //   if (cursors.up.isDown) {//Player is pressing up
    //     this.jumped ? particles.jumpParticles(ctx, player) : null;
    //     if (this.facingRight) {
    //       player.setVelocity(-this.wallJumpVelocity.x,-this.wallJumpVelocity.y)
    //     } else {
    //       player.setVelocity(this.wallJumpVelocity.x,-this.wallJumpVelocity.y)
    //     }
    //   } else {
    //     player.setVelocityY(0)
    //   }
    // } else {
    //   // console.log('wall not grabbed');
    //   player.body.allowGravity = true;//When player is no longer against wall switch gravity back on
    // }

  } //buttonInputHandler

  setDirection (key, entity) { // Set sprite animation based on player input
    entity.anims.play(key, true);
  }

}
