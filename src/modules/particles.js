import 'phaser';
import gamestore from 'data/gamestore.js';

export default class Particles {

  jumpParticles (ctx, player) {
    var config = {
        speed: { min: 100, max: 0 },
        angle: { min: 0, max: 360 },
        scale: { min: 0.5, max: 0.65},
        quantity: 2,
        gravityY: 450,
        scale: { start: 0.5, end: 0 },
        alpha: { start: 0.65, end: 0.2 },
        bounce: 0.8,
        velocity: { min: 2500, max: 5000 },
        blendMode: 'ADD'
    }
    const dustEmitter = ctx.jumpParticles.createEmitter(config);
    dustEmitter.on = true;
    dustEmitter.startFollow(player)
    dustEmitter.followOffset.y = 20;
    setTimeout(function(){
     dustEmitter.explode()
   },500)
  }

}
