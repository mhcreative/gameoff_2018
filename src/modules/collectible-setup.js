import 'phaser';
import gamestore from 'data/gamestore.js';
import CharacterMovement from 'modules/character-movement.js';

const characterMovement = new CharacterMovement();

export default class CollectibleSetup {

  starsSetup (ctx, key, repeat, x, y, stepX, stepY, min, max) {

    const stars = ctx.physics.add.group({
      key: key,
      repeat: repeat,// Repeat the instance of star x times
      setXY: { x: x, y: y, stepX: stepX, stepY: stepY } // Stagger the stars. Starts out 70 from the first one at x:<number>
    })

    stars.children.iterate(function (child) { // Like a forEach statement, each item in the stars group fall down and bounce at different rates.
      child.setBounceY(Phaser.Math.FloatBetween(min, max));
    })

    return stars

  }

  collectStar (player, star) { // When user moves over/collects star
    star.disableBody(true, true);
    this.sfx.pickup.play();
    gamestore.physics.superJump = true;
    gamestore.metrics.score += 10;
    this.scoreText.setText('Score: ' + gamestore.metrics.score)

    if (this.stars.countActive(true) === 0) {
      this.stars.children.iterate(function (child) {
        child.enableBody(true, child.x, 0, true, true);
      })

      var x = (this.player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400)

      var bomb = this.bombs.create(x, 16, 'bomb')
      bomb.setBounce(1)
      bomb.setCollideWorldBounds(true)
      bomb.setVelocity(Phaser.Math.Between(-200, 200), 20)
      bomb.allowGravity = false;
    }
  }

  setupItems (ctx, imageKey, dynamicLayerName, tileIndex) {
    const itemTiles = ctx.map.addTilesetImage(imageKey);
    if(itemTiles) {
      const itemLayer = ctx.map.createDynamicLayer(dynamicLayerName, itemTiles, 0, 0);
      itemLayer.setTileIndexCallback(tileIndex , this.collectItem, ctx);
      return itemLayer;
    }
  }

  collectItem (sprite, tile) {
    this.itemLayer.removeTileAt(tile.x, tile.y); // remove the tile/coin
    const duster = this.add.sprite(this.player.x, this.player.y-70, 'jumpDust')
    duster.anims.play('highJump')
    this.sfx.pickup.play();
  }

}
