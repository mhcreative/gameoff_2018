import 'phaser';
import gamestore from 'data/gamestore';
// import Npcs from 'modules/Npcs.js';

// const npcs = new Npcs()

export default class GameplayStates {

  failState (ctx, camera) {
    if (!gamestore.gameplayStates.failed) {
      // Pause music
      // Set gameplay state to failed
      // Drop player through bottom of screen
      // Shake camera
      // Fade out camera
      // Restart scene/level
      ctx.sfx.house.stop();
      gamestore.gameplayStates.failed = true;
      ctx.player.setCollideWorldBounds(false)
      ctx.physics.pause();

      const duster = ctx.add.sprite(ctx.player.x, ctx.player.y-70, 'jumpDust')
      duster.anims.play('highJump')
      ctx.player.setAlpha(0)

      camera.shake(500)
      camera.on('camerashakecomplete', function () {
          camera.fade(250)
      })
      camera.on('camerafadeoutcomplete', function () {
        ctx.scene.restart()
      })
    }

  }

  successState (ctx, camera) {
    ctx.player.anims.play('idle');
    gamestore.gameplayStates.failed = false;
    if (!gamestore.gameplayStates.success) {
      ctx.player.setVelocityX(0);
      gamestore.gameplayStates.success = true;
      ctx.cursors.right.enabled = false;
      ctx.cursors.left.enabled = false;
      ctx.cursors.up.enabled = false;
  
      camera.shake(250)
      camera.on('camerashakecomplete', function () {
          var rect = new Phaser.Geom.Rectangle(0, 0, gamestore.gamedata.canvasWidth+1000, gamestore.gamedata.canvasHeight+1000);
          var graphics = ctx.add.graphics({ fillStyle: { color: 0x000000 } });
          graphics.fillRectShape(rect);
          graphics.alpha = 0.5;
          ctx.success = ctx.add.sprite(gamestore.gamedata.canvasWidth/2, gamestore.gamedata.canvasHeight/1.35, "successText");
          ctx.sfx.house.stop();
          ctx.sfx.success.play();
          setTimeout(function() {
            camera.fade(500);
            camera.on('camerafadeoutcomplete', function () {
              ctx.scene.start('IntroScene')
            })
          },3000)
      })
    }

  }

}
