import 'phaser';
import gamestore from 'data/gamestore.js';

export default class CameraSetup {

  camSetup (ctx, map, bgColour) {
    // set bounds so the camera won't go outside the game world
    ctx.cameras.main.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
    // make the camera follow the player
    ctx.cameras.main.startFollow(ctx.player);
    // set background color, so the sky is not black
    ctx.cameras.main.setBackgroundColor(bgColour);

    return ctx.cameras.main
  }

}
