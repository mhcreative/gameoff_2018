import 'phaser';
import gamestore from 'data/gamestore.js';
import GameplayStates from 'modules/gameplay-states';

const gameplayStates = new GameplayStates()

export default class Npcs {

  // Player hits bomb
  hitBomb (player, bomb) {
    this.physics.pause();
    this.player.setTint(0xff0000);
    gameplayStates.failState(this, this.cameraMain);
    const duster = this.add.sprite(this.player.x, this.player.y-70, 'jumpDust')
    duster.anims.play('highJump')
  }

  entityDestroy (entity, spriteKey, soundfx) {
    entity.forEach(
      (ent) => {
        const duster = this.add.sprite(ent.x, ent.y-70, 'jumpDust')
        duster.anims.play(spriteKey)
        if (soundfx) {
          if (!this.sfx[soundfx].isPlaying) {
            this.sfx[soundfx].play();
          }
        }
        ent.destroy()
      }
    )
  }

  enemyProjectile (ctx, enemy) {
    // console.log(this)
    var projectile = ctx.bombs.create(ctx.enemy1.x, ctx.enemy1.y-20, 'bomb')
    projectile.body.allowGravity = false;
    projectile.setBounce(0.85)
    projectile.setVelocity(-200, 0)
    return projectile
  }

  entityPatrol (ctx, entity) {
    entity.flipX = false;
    ctx.tweens.add({
        targets: entity,
        x: "+=100",
        duration: 2500,
        ease: 'Power0',
        repeat: -1,
        flipX: true,
        yoyo: true
    });
   }

}
