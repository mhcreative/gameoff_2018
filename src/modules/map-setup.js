import 'phaser';
import gamestore from 'data/gamestore.js';

export default class MapSetup {

  sceneMap (ctx, mapdata) {// Setup up map
    // Map
    const map = ctx.make.tilemap({key: mapdata.tilemapKey});

    // tiles for the ground layer - tilesets.name`
    const groundTiles = map.addTilesetImage(mapdata.spriteSheetKey);

    // Create Background as static layer first
    const bg = map.createStaticLayer(mapdata.tileBgLayerName, groundTiles, 0, 0);

    // if () {}
    const bg2 = map.createStaticLayer('bg2', groundTiles, 0, 0);

    // create the ground layer - layers[i].name
    const groundLayer = map.createDynamicLayer(mapdata.tileGroundLayerName, groundTiles, 0, 0);

    // entities can collide with this layer
    groundLayer.setCollisionByProperty({ collides: true }) // either above or: groundLayer.setCollisionByExclusion([-1]);

    // set the boundaries of our game world
    ctx.physics.world.bounds.width = groundLayer.width;
    ctx.physics.world.bounds.height = groundLayer.height;

    return {map:map, groundTiles:groundTiles, groundLayer:groundLayer}
  }

}
