import 'phaser';
import gamestore from 'data/gamestore';

export default class Zones {

  zoneTest () {
    console.log('zones')
  }

  createZone (ctx, x, y, width, height) {
    const zone = ctx.add.zone(x, y).setSize(width, height);
    ctx.physics.world.enable(zone);
    zone.body.moves = false;

    return zone
  }

}
