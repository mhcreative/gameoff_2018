export default {
  physics : {
    gravity: 1500,
    default: 'arcade',
    debug: false,
    baseleftRightSpd: 200, // Base speed
    leftRightSpd: 200, // (Initial value = baseleftRightSpd) Movement speed
    runAcceleration: 250, // Running acceleration rate
    runDeceleration: 600, // (runAcceleration / 10) Running deceleration rate
    runSpdLimit: 500, // Maxixum running speed
    facingRight: true, // true = player facing right
    jumpVelocity: 500, // Velocity of jump
    superJumpVelocity: 500 * 1.7, // Velocity of Super-jump
    skidFlipVelocity: {x: 500*0.25, y: 500*1.5},
    wallJumpVelocity: {x: 150, y: 500 * 1.5}, // Velocity of Super-jump
    superJump: false, // true = Super-jump is active
    runningFrameRate: 10, // Framerate of animation of running sprite
    playerWidth: 46, // Sprite Width
    playerHeight: 38, // Sprite Height
    jumped: false, // Player is holding jump button
  }
}
