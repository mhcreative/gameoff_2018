import gamedataStore from './gamedata';
import physicsStore from './physics';
import mapStore from './map';
import entitiesStore from './entities';
import audioStore from './audio';
import sceneStore from './scenes';


export default {
    gamedata: gamedataStore.gamedata,
    physics: physicsStore.physics,
    metrics: {
      score: 0
    },
    gameplayStates: {
      failed: false,
      success: false
    },
    mapData: mapStore.mapData,
    entities: entitiesStore.entities,
    audio: audioStore.audio,
    scenes: sceneStore
  }
