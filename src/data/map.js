export default {
  mapData: {
    playground: {
      tilemapKey: 'playground',
      jsonFilePath: 'assets/levels/playground/playground.json',
      spriteSheetKey: 'cave',
      spriteSheetFilePath: 'assets/levels/tilesets/cave.png',
      frameWidth: 64,
      frameheight: 64,
      tileGroundLayerName: 'playground',
      tileBgLayerName: 'bg'
    },
    house: {
      tilemapKey: 'house',
      jsonFilePath: 'assets/levels/house/house.json',
      spriteSheetKey: 'house',
      spriteSheetFilePath: 'assets/levels/tilesets/levels.png',
      frameWidth: 64,
      frameheight: 64,
      tileGroundLayerName: 'house',
      tileBgLayerName: 'bg'
    },
    intro: {
      tilemapKey: 'intro',
      jsonFilePath: 'assets/levels/intro/intro.json',
      spriteSheetKey: 'cave',
      spriteSheetFilePath: 'assets/levels/tilesets/cave.png',
      frameWidth: 64,
      frameheight: 64,
      tileGroundLayerName: 'intro',
      tileBgLayerName: 'bg'
    }
  }
}
