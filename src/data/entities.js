export default {
  entities: {
    player: {
      spriteKey: 'heroIdle',
      physics: true,
      bounce: 0,
      setCollideWorldBounds: true,
      spawnPoint: 'spawn',
      startVisible: true,
      playerHitboxConfig: {
        idle: {
          size: {
            width: 26,
            height: 55
          },
          offset: {
            x: 0,
            y: 25
          }
        },
        jumping: {
          size: {
            width: 24,
            height: 35
          },
          offset: {
            x: 10,
            y: 45
          }
        },
        running: {
          size: {
            width: 26,
            height: 55
          },
          offset: {
            x: 20,
            y: 25
          }
        },
        wallgrab: {
          size: {
            width: 39,
            height: 55
          },
          offset: {
            x: 0,
            y: 0
          }
        }
      }
    },
    cat: {
      spriteKey: 'catIdle',
      physics: true,
      spriteFrameWidth: 38,
      spriteFrameHeight: 30,
      bounce: 0,
      setCollideWorldBounds: true,
      spawnPoint: 'cat',
      startVisible: true
    },
    catHero: {
      spriteKey: 'catHeroIdle',
      physics: true,
      spriteFrameWidth: 39,
      spriteFrameHeight: 72,
      bounce: 0,
      setCollideWorldBounds: true,
      spawnPoint: 'spawn',
      startVisible: true
    },
    wizard: {
      spriteKey: 'wizardIdle',
      physics: false,
      spriteFrameWidth: 160,
      spriteFrameHeight: 160,
      bounce: 0,
      setCollideWorldBounds: true,
      spawnPoint: 'wizardSpawn',
      startVisible: true
    },
    dust: {
      spriteKey: 'dust',
      physics: false,
      bounce: 0,
      setCollideWorldBounds: false,
      spawnPoint: '',
      startVisible: false
    },
    jumpDust: {
      spriteKey: 'jumpDust',
      physics: false,
      bounce: 0,
      setCollideWorldBounds: false,
      spawnPoint: '',
      startVisible: false
    },
    koopa: {
      spriteKey: 'koopa',
      physics: true,
      bounce: 0.2,
      setCollideWorldBounds: true,
      spawnPoint: 'enemySpawn',
      startVisible: true
    },
    dm: {
      spriteKey: 'dm',
      physics: true,
      bounce: 0.2,
      setCollideWorldBounds: true,
      spawnPoint: 'enemySpawn',
      startVisible: true
    },
    bat: {
      spriteKey: 'bat',
      physics: true,
      bounce: 0.2,
      setCollideWorldBounds: true,
      spawnPoint: 'batSpawn',
      startVisible: true
    },
    boobyTrap: {
      spriteKey: 'trigger',
      physics: true,
      bounce: 0,
      setCollideWorldBounds: true,
      spawnPoint: 'boobyTrap',
      startVisible: true
    },
    exit: {
      spriteKey: 'exit',
      physics: true,
      bounce: 0,
      setCollideWorldBounds: true,
      spawnPoint: 'exit',
      startVisible: true
    }
  }
}
