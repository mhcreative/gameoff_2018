export default {
  audio: {
    sfx: [
      {
        name: 'jump',
        src: 'assets/audio/jump.ogg',
        volume: 0.05,
        sound: null,
        loop: false
      },
      {
        name: 'pickup',
        src: 'assets/audio/pickup.wav',
        volume: 0.05,
        sound: null,
        loop: false
      },
      {
        name: 'house',
        src: 'assets/audio/houseFinal.ogg',
        volume: 0.05,
        sound: null,
        loop: true
      },
      {
        name: 'success',
        src: 'assets/audio/success.ogg',
        volume: 0.03,
        sound: null,
        loop: false
      }
    ],
    music: []
  }
}
