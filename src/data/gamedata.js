export default {
  gamedata: {
    canvasWidth: 640,
    canvasHeight: 360,
    defaultCameraBgColour: '#253d45'
  }
}
