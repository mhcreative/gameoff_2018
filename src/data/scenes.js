export default {
    dialogueData: {
        webfont: 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js',
        fontFamilies: ['Contrail One'],
        webSafeFont: 'Arial',
        fontSize: 16,
        lineSpacing: 10,
        color: 'white',
        backgroundColor: 'transparent',
        backgroundImage: 'assets/textures/water.png',
        wordWrap: {
            width: 320,
            useAdvancedWrap: true
        },
        shadow: {
            color: '#000000',
            fill: true,
            offsetX: 2,
            offsetY: 2,
            blur: 8
        },
        padding: {
            left: 15,
            top: 15
        },
        catConfig: {
          colour: 'white',
          bgColour: 'Chocolate'
        },
        WizardConfig: {
          colour: 'white',
          bgColour: 'MidnightBlue'
        }
    },
    intro: {
      dialogueCat: {
        script: {
          line1: "Meow"
        },
        position: { x: 510, y: 420},
        visible: true
      },
      dialogueWizard: {
        script: {
          line1: "I sense that... you desire to be reunited with your master",
          line2: "Yes, I can make it so. I must warn you, the powers I can bestow onto you will put you in strife. Are you sure?",
          line3: "...Very well",
          line4: "With your new abilities, go forth and find your master. Be careful my spell has affected other creatures. The path before you is treacherous"
        },
        position: { x: 310, y: 350},
        visible: true
      }
    }
}
