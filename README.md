# Platformer   
##### Requirements   
Node.js  
Yarn or NPM

##### Install and run
Run `yarn install` or `npm install` to install dependencies    
Run `yarn start` or `npm start`
Server will live at `localhost:8080`

##### Deploy  
Run `yarn run build` or `npm run build`
There should be a build folder once the build process is complete

Run `yarn run deploy` or `npm run deploy` to build the project and deploy it to [https://gameoff2018.surge.sh](https://gameoff2018.surge.sh). The domain is configured inside the [CNAME](https://bitbucket.org/mhcreative/gameoff_2018/src/master/CNAME) file with HTTPS enforced.

You can always manually deploy by calling `surge build`.

##### Resources
Enemy type reference:    https://www.reddit.com/r/gamedesign/comments/5pv68k/platformer_enemytypes_listreference/?utm_content=title&utm_medium=front&utm_source=reddit&utm_name=gamedesign

##### Team Members   
- [Moe Hammoud - Developer](http://moehammoud.com)
- [Stephan Max - Developer](https://stephanmax.is)
- [Aaron MacFadyen - SFX and Music](https://soundcloud.com/di0de)
